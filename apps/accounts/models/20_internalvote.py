# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# import datetime
from django.db import models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User
from wioframework.amodels import wideiomodel, wideio_timestamped, get_dependent_models, wideio_timestamped_autoexpiring, wideio_publishable, wideio_action, wideio_owned, wideio_commentable, wideio_setnames
from wioframework import decorators as dec

from wioframework.fields import JSONField
import settings

@wideio_commentable()
@wideio_publishable()
@wideio_timestamped
@wideiomodel
class InternalVote(models.Model):

    """
    A Vote
    """
    name = models.CharField(
        blank=True,
        max_length=128,
        db_index=True,
        unique=True
    )

    question = models.TextField(blank=False)
    options = models.TextField(blank=False,default="yes,no",help_text="Comma separated list of options")
    total_equity=models.IntegerField(default=-1) #< Indicative value providing the equity at the time of the vote
    total_investors=models.IntegerField(default=-1) #< Indicative value providing the number of people in the company at the time of the vote
    finishes_at=models.DateTimeField(null=True)
    display_individual_votes=models.BooleanField(default=False)
    display_submitted_results_during_vote=models.BooleanField(default=False)

    def options_list(self):
        l1=map(lambda x:x.strip(),self.options.split(","))
        return zip(range(len(l1)),l1)

    def results(self):
        l1=map(lambda x:x.strip(),self.options.split(","))
        ires=[0]*len(l1)
        fres=[0.0]*len(l1)
        bs=InternalVoteBulletin.objects.filter(vote=self)
        ts=0
        for b in bs:
            ires[b.value]+=b.shares_involved
            ts+=b.shares_involved
        if ts==0:
            ts=1
        for v in range(len(l1)):
            fres[v]=float(ires[v])/ts
        return zip(range(len(l1)),l1,ires,fres)

    def get_all_references(self,stack):
        return []

    def __unicode__(self):
        return self.name

    def on_add(self,request):
        from accounts.models import UserAccount
        investors=UserAccount.objects.filter(is_investor=True)
        total_equity=0
        for i in investors:
            total_equity+=i.get_investor_profile().total_shares
        self.total_investors=len(investors)
        self.total_equity=total_equity

    def all_investors(self):
        from accounts.models import UserAccount
        return UserAccount.objects.filter(is_investor=True)

    def send(self,request):
        from accounts.models import UserAccount
        from network.models import Announcement
        Announcement.create_internal(request.user, "WIDE IO Team", content=self, comments_opened=False, to=UserAccount.objects.filter(is_active=True,is_investor=True))

    class WIDEIO_Meta:
        NO_DRAFT = True
        permissions = dec.perm_write_for_staff_only
        form_exclude=["total_equity","total_investors"]
        DISABLE_VIEW_ACCOUNTING = True
        class Actions:
            @wideio_action(possible=lambda s,r:r.user.is_staff)
            def send_to_all_active_users(self,request):
                self.send(request)
                return "alert('done');"

        MOCKS = {
            'default':[
                {
                    'name' : "internalvote-0000",
                    'question' : "Would you like tea ?",
                    'options' : "yes,no",
                    'total_equity':0,
                    'total_investors':0
                },
                {
                    'name' : "internalvote-0001",
                    'question' : "Would you like coffee ?",
                    'options' : "yes,no",
                    'total_equity':1000,
                    'total_investors':2
                }
            ]
        }