#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *


@wideio_publishable()
@wideiomodel
class InvestorProfile(models.Model):
    user_profile = models.OneToOneField('UserAccount', related_name="investor_profile")
    total_shares = models.IntegerField(default=0, null=True)
    total_invested = models.IntegerField(default=0, null=True)

    def get_all_references(self):
        return []

    @staticmethod
    def _on_load_postprocess(M):
        def is_investor(self):
            return InvestorProfile.objects.filter(user_profile=self).count() != 0

        def is_investor_setter(self, value):
            if value and not self.is_investor:
                sp = InvestorProfile()
                sp.user_profile = self
                sp.save()
            if not value and self.is_investor:
                return InvestorProfile.objects.filter(user_profile=self).delete()

        def get_investor_profile(self):
            return InvestorProfile.objects.filter(user_profile=self)[0]

        def invoke_investor_profile(self):
            if not self.is_investor:
                self.is_investor = True
            return InvestorProfile.objects.filter(user_profile=self)[0]

        def set_investor(self):
            useraccount.WIDEIO_Meta.Actions.set_investor.of(self, None)

        useraccount = filter(lambda m: m.__name__ == 'UserAccount', M)[0]
        useraccount.is_investor = property(is_investor, is_investor_setter)
        useraccount.get_investor_profile = get_investor_profile
        useraccount.invoke_investor_profile = invoke_investor_profile
        useraccount.set_investor = set_investor

    class WIDEIO_Meta:
        permissions = dec.perm_write_for_staff_only
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING = True
        CAN_TRANSFER = False
        MOCKS = {
            'default': [
                {
                    'user_profile': 'user0010',
                    'total_shares': 200,
                    'total_invested': 3000
                }
            ]
        }
