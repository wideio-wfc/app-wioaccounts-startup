# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# import datetime
from django.db import models
from wioframework.amodels import wideiomodel, wideio_timestamped, get_dependent_models, wideio_timestamped_autoexpiring, wideio_publishable, wideio_action, wideio_owned, wideio_commentable, wideio_setnames
import datetime

@wideio_owned(personal=2)
@wideio_timestamped
@wideiomodel
class InternalVoteBulletin(models.Model):
    """
    A Vote
    """
    vote = models.ForeignKey('accounts.InternalVote',blank=False)
    shares_involved=models.IntegerField(default=-1)
    value=models.IntegerField()

    def on_add(self,request):
       # delete all previous votes
       if self.vote.finishes_at < datetime.datetime.now():
           self.delete()
           raise Exception, "Vote has expired"
       InternalVoteBulletin.objects.filter(vote=self.vote,owner=request.user).delete()
       InternalVoteBulletin.objects.filter(vote=self.vote,owner=None).delete()
       MS=request.user.get_investor_profile().total_shares
       if self.shares_involved<0 or self.shared_involved>MS:
           self.shares_involved=MS
       self.save()
       return {'_redirect':self.vote.get_view_url()}

    class WIDEIO_Meta:
        NO_DRAFT = True
        #permissions = dec.perm_write_for_staff_only
        DISABLE_VIEW_ACCOUNTING = True
#        class Actions:
#            @wideio_action(possible=lambda s,r:r.user.is_staff)
#            def send_to_all_active_users(self,request):
#                self.send(request)
#                return "alert('done');"
        MOCKS={
            'default':[

            ]
        }